﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace BuildUp.Droid
{
    [Activity(Label = "BuildUp", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            #region Screen size calculations
            Android.Content.Res.TypedArray styledAttributes = Theme.ObtainStyledAttributes(new int[] { Android.Resource.Attribute.ActionBarSize });
            int navigationBarHeight = (int)(styledAttributes.GetDimension(0, 0) / Resources.DisplayMetrics.Density);
            styledAttributes.Recycle();

            int statusBarHeight = 0, totalHeight = 0, contentHeight = 0;
            int resourceId = Resources.GetIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0)
            {
                statusBarHeight = Resources.GetDimensionPixelSize(resourceId);

                totalHeight = Resources.DisplayMetrics.HeightPixels;
                contentHeight = totalHeight - statusBarHeight - navigationBarHeight;
            }

            var screenWidth = Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density;
            var screenHeight = contentHeight / Resources.DisplayMetrics.Density;

            if (screenHeight > screenWidth)
            {
                App.ScreenSize = new Xamarin.Forms.Size(
                    screenWidth,
                    screenHeight);
            }
            else
            {
                App.ScreenSize = new Xamarin.Forms.Size(
                    screenHeight,
                    screenWidth);
            }
            #endregion

            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}