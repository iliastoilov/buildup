﻿using System;

namespace BuildUp.Core
{
    public class Building
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
