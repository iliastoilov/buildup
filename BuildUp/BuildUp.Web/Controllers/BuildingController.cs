﻿using System;
using System.Collections.Generic;
using System.Linq;
using BuildUp.Core;
using Microsoft.AspNetCore.Mvc;

namespace BuildUp.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BuildingController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Building> Get()
        {
            var buildings = new List<Building>()
            {
                new Building() { ID = 1, Name = "Building 1" },
                new Building() { ID = 2, Name = "Building 2" },
                new Building() { ID = 3, Name = "Building 3" },
            };

            return buildings;
        }
    }
}
