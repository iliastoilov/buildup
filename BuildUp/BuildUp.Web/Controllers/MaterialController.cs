﻿using System;
using System.Collections.Generic;
using BuildUp.Core;
using Microsoft.AspNetCore.Mvc;

namespace BuildUp.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MaterialController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Material> Get()
        {
            var materials = new List<Material>();

            for (int i = 0; i < 20000; i++)
            {
                materials.Add(new Material() { ID = i, Name = "Material " + i, Detail = "Lorem ipsum dolor sit amet" });
            }

            return materials;
        }
    }
}
