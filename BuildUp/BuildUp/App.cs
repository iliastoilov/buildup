﻿using System;
using BuildUp.Views.Buildings;
using Xamarin.Forms;

namespace BuildUp
{
    public class App : Application
    {
        public static Size ScreenSize { get; set; }

        public App()
        {
            AppSettings.Setup();
            MainPage = new NavigationPage(new BuildingsPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
