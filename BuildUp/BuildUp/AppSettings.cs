﻿using System;
using Xamarin.Forms;

namespace BuildUp
{
    public static class AppSettings
    {
        public static string WebApiURL;

        public static void Setup()
        {
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    WebApiURL = "https://10.0.2.2:26265/";
                    break;
                case Device.iOS:
                    WebApiURL = "https://localhost:26265/";
                    break;
            }
        }
    }
}
