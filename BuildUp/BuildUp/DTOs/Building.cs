﻿using System;
namespace BuildUp.DTOs
{
    public class Building
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
