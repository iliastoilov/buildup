﻿using System;
namespace BuildUp.DTOs
{
    public class Material
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
    }
}
