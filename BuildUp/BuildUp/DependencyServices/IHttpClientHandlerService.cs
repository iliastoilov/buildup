﻿using System;
using System.Net.Http;

namespace BuildUp.DependencyServices
{
    public interface IHttpClientHandlerService
    {
        HttpClientHandler GetInsecureHandler();
    }
}
