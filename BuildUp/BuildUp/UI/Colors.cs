﻿using System;
using Xamarin.Forms;

namespace BuildUp.UI
{
    public static class Colors
    {
        public static Color AppBackground = Color.AliceBlue;
        public static Color Accent = Color.CadetBlue;
    }
}