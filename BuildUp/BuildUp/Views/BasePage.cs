﻿using BuildUp.UI;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace BuildUp.Views
{
    public class BasePage : ContentPage
    {
        protected double ScreenWidth { get; private set; }
        protected double ScreenHeight { get; private set; }

        public BasePage()
        {
            On<iOS>().SetUseSafeArea(true);
            BackgroundColor = Colors.AppBackground;
            this.ScreenWidth = App.ScreenSize.Width;
            this.ScreenHeight = App.ScreenSize.Height;
        }
    }
}