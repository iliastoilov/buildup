﻿using System;
using BuildUp.DTOs;
using BuildUp.UI;
using BuildUp.Views.Materials;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using Telerik.XamarinForms.Input;
using Telerik.XamarinForms.Primitives;
using Xamarin.Forms;

namespace BuildUp.Views.Buildings
{
    public class BuildingsPage : BasePage
    {
        public BuildingsPage()
        {
            Title = "Buildings";
            BindingContext = new BuildingsPageModel();

            #region Controls
            RadBusyIndicator busyIndicator = new RadBusyIndicator()
            {
                AnimationContentColor = Colors.Accent,
                WidthRequest = 50,
                HeightRequest = 50,
                AnimationContentWidthRequest = 50,
                AnimationContentHeightRequest = 50,
                IsBusy = true,
            };

            var listView = new RadListView
            {
                ItemTemplate = new DataTemplate(() =>
                {
                    var cell = new ListViewTextCell
                    {
                        TextColor = Color.Black,
                        DetailColor = Color.Gray,
                    };

                    cell.SetBinding(ListViewTextCell.TextProperty, new Binding(nameof(Building.Name)));

                    return cell;
                }),
                LayoutDefinition = new ListViewLinearLayout { ItemLength = 60 },
                VerticalOptions = LayoutOptions.FillAndExpand,
                SelectionMode = Telerik.XamarinForms.DataControls.ListView.SelectionMode.Single,
            };

            var btnGetBuildings = new RadButton()
            {
                Text = "Get buildings",
                FontAttributes = FontAttributes.Bold,
                BackgroundColor = Colors.AppBackground,
                BorderColor = Colors.Accent,
                TextColor = Colors.Accent,
                BorderWidth = 3,
                Margin = new Thickness(10)
            };
            #endregion

            #region Layout
            var grid = new CustomGrid();
            grid.AddColumn(1, GridUnitType.Star);
            grid.AddRow(1, GridUnitType.Star);
            grid.AddRow(1, GridUnitType.Auto);

            grid.Add(busyIndicator, 0, 1, 0, 1);
            grid.Add(listView, 0, 1, 0, 1);
            grid.Add(btnGetBuildings, 0, 1, 1, 2);

            Content = grid;
            #endregion

            #region Events
            listView.ItemTapped += ListView_ItemTapped;
            #endregion

            #region Bindings
            busyIndicator.SetBinding(RadBusyIndicator.IsVisibleProperty, nameof(BuildingsPageModel.IsBusy));
            listView.SetBinding(RadListView.ItemsSourceProperty, nameof(BuildingsPageModel.Buildings));
            btnGetBuildings.SetBinding(Button.CommandProperty, nameof(BuildingsPageModel.GetBuildingsCommand));
            #endregion
        }

        private async void ListView_ItemTapped(object sender, ItemTapEventArgs e)
        {
            if (sender is RadListView listView)
            {
                await App.Current.MainPage.Navigation.PushAsync(new MaterialsPage());
                listView.SelectedItem = null;
            }
        }
    }
}
