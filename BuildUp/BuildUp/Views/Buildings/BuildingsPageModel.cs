﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using BuildUp.DTOs;
using BuildUp.Web;
using Xamarin.Forms;

namespace BuildUp.Views.Buildings
{
    public class BuildingsPageModel : BaseViewModel
    {
        public ObservableCollection<Building> Buildings { get; set; }

        public bool IsBusy { get; set; }

        public Command GetBuildingsCommand { get; private set; }

        public BuildingsPageModel()
        {
            GetBuildingsCommand = new Command(GetBuildings);
        }

        private async void GetBuildings()
        {
            IsBusy = true;

            Buildings = new ObservableCollection<Building>();

            await Task.Delay(1000);

            var request = new GetBuildingsRequest();

            var result = await request.ExecuteWithCustomResultAsync<List<Building>>();

            Buildings = new ObservableCollection<Building>(result);

            IsBusy = false;
        }
    }
}
