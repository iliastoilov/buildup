﻿using System;
using BuildUp.DTOs;
using BuildUp.UI;
using Telerik.XamarinForms.DataControls;
using Telerik.XamarinForms.DataControls.ListView;
using Telerik.XamarinForms.DataGrid;
using Telerik.XamarinForms.Primitives;
using Xamarin.Forms;

namespace BuildUp.Views.Materials
{
    public class MaterialsPage : BasePage
    {
        public MaterialsPage()
        {
            Title = "Materials";
            BindingContext = new MaterialsPageModel();

            #region Controls
            RadBusyIndicator busyIndicator = new RadBusyIndicator()
            {
                AnimationContentColor = Colors.Accent,
                WidthRequest = 50,
                HeightRequest = 50,
                AnimationContentWidthRequest = 50,
                AnimationContentHeightRequest = 50,
                IsBusy = true,
            };

            var datagrid = new RadDataGrid();
            #endregion

            #region Layouto
            var grid = new CustomGrid();
            grid.AddColumn(1, GridUnitType.Star);
            grid.AddRow(1, GridUnitType.Star);

            grid.Add(busyIndicator, 0, 1, 0, 1);
            grid.Add(datagrid, 0, 1, 0, 1);

            Content = grid;
            #endregion

            #region Bindings
            busyIndicator.SetBinding(RadBusyIndicator.IsVisibleProperty, nameof(MaterialsPageModel.IsBusy));
            datagrid.SetBinding(RadDataGrid.ItemsSourceProperty, nameof(MaterialsPageModel.Materials));
            #endregion
        }
    }
}
