﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using BuildUp.DTOs;
using BuildUp.Web;

namespace BuildUp.Views.Materials
{
    public class MaterialsPageModel : BaseViewModel
    {
        public ObservableCollection<Material> Materials { get; set; }

        public bool IsBusy { get; set; }

        public MaterialsPageModel()
        {
            Init();
        }

        private async void Init()
        {
            IsBusy = true;

            var request = new GetMaterialsRequest();

            var result = await request.ExecuteWithCustomResultAsync<List<Material>>();

            Materials = new ObservableCollection<Material>(result);

            IsBusy = false;
        }
    }
}
