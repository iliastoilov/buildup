﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BuildUp.DependencyServices;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace BuildUp.Web
{
    public class BaseWebRequest
    {
        public bool IsAuthorized = true;
        public string URL;
        public string WebApiURL;
        public HttpMethod Method = HttpMethod.Get;
        public object Parameters;
        public int Timeout = 20000;

        public BaseWebRequest()
        {
            this.WebApiURL = AppSettings.WebApiURL;
        }

        public async Task<T> ExecuteWithCustomResultAsync<T>() where T : class
        {
            HttpWebRequest.DefaultCachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.BypassCache);

            HttpResponseMessage response = new HttpResponseMessage();

            switch (Method.Method)
            {
                case "POST":
                    response = await PostAsync(JsonConvert.SerializeObject(Parameters));
                    break;
                case "GET":
                    response = await GetAsync(Parameters != null ? Parameters.ToString() : "");
                    break;
                case "DELETE":
                    response = await DeleteAsync(JsonConvert.SerializeObject(Parameters));
                    break;
                case "PUT":
                    response = await PutAsync(JsonConvert.SerializeObject(Parameters));
                    break;
            }

            try
            {
                var content = await response.Content.ReadAsStringAsync();

                Console.WriteLine(content);

                var result = JsonConvert.DeserializeObject<T>(content);

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private async Task<HttpResponseMessage> PostAsync(string parameters)
        {
            string uri = "";

            using (HttpClient client = new HttpClient(DependencyService.Get<IHttpClientHandlerService>().GetInsecureHandler()))
            {
                try
                {
                    StringContent content = new StringContent(parameters, Encoding.UTF8, "application/json");

                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri(WebApiURL + URL),
                        Method = Method,
                    };

                    request.Content = content;

                    uri = request.RequestUri.AbsoluteUri;
                    Console.WriteLine("POST " + uri);
                    return await client.SendAsync(request);
                }
                catch (OperationCanceledException cancelled)
                {
                    Console.WriteLine("FAILED " + uri + "\n" + cancelled.Message);
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.RequestTimeout };
                }
                catch (Exception ex)
                {
                    Console.WriteLine("FAILED " + uri + "\n" + ex.Message);
                    return default(HttpResponseMessage);
                }
            }
        }

        private async Task<HttpResponseMessage> GetAsync(string parameters)
        {
            string uri = "";

            using (HttpClient client = new HttpClient(DependencyService.Get<IHttpClientHandlerService>().GetInsecureHandler()))
            {
                try
                {
                    var request = new HttpRequestMessage()
                    {
                        Method = Method,
                    };

                    if (string.IsNullOrWhiteSpace(parameters) || parameters == "null")
                    {
                        request.RequestUri = new Uri(WebApiURL + URL + "/");
                    }
                    else
                    {
                        request.RequestUri = new Uri(WebApiURL + URL + parameters);
                    }

                    uri = request.RequestUri.AbsoluteUri;
                    Console.WriteLine("GET " + uri);
                    return await client.SendAsync(request);
                }
                catch (OperationCanceledException cancelled)
                {
                    Console.WriteLine("FAILED " + uri + "\n" + cancelled.Message);
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.RequestTimeout };
                }
                catch (Exception ex)
                {
                    Console.WriteLine("FAILED " + uri + "\n" + ex.Message);
                    return default(HttpResponseMessage);
                }
            }
        }

        private async Task<HttpResponseMessage> DeleteAsync(string parameters)
        {
            string uri = "";

            using (HttpClient client = new HttpClient(DependencyService.Get<IHttpClientHandlerService>().GetInsecureHandler()))
            {
                try
                {
                    StringContent content = new StringContent(parameters, Encoding.UTF8, "application/json");

                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri(WebApiURL + URL),
                        Method = Method,
                    };

                    request.Content = content;

                    uri = request.RequestUri.AbsoluteUri;
                    Console.WriteLine("DELETE " + uri);
                    return await client.SendAsync(request);
                }
                catch (OperationCanceledException cancelled)
                {
                    Console.WriteLine("FAILED " + uri + "\n" + cancelled.Message);
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.RequestTimeout };
                }
                catch (Exception ex)
                {
                    Console.WriteLine("FAILED " + uri + "\n" + ex.Message);
                    return default(HttpResponseMessage);
                }
            }
        }

        private async Task<HttpResponseMessage> PutAsync(string parameters)
        {
            string uri = "";

            using (HttpClient client = new HttpClient(DependencyService.Get<IHttpClientHandlerService>().GetInsecureHandler()))
            {
                try
                {
                    StringContent content = new StringContent(parameters, Encoding.UTF8, "application/json");

                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri(WebApiURL + URL),
                        Method = Method,
                    };

                    request.Content = content;

                    uri = request.RequestUri.AbsoluteUri;
                    Console.WriteLine("PUT " + uri);
                    return await client.SendAsync(request);
                }
                catch (OperationCanceledException cancelled)
                {
                    Console.WriteLine("FAILED " + uri + "\n" + cancelled.Message);
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.RequestTimeout };
                }
                catch (Exception ex)
                {
                    Console.WriteLine("FAILED " + uri + "\n" + ex.Message);
                    return default(HttpResponseMessage);
                }
            }
        }
    }
}
