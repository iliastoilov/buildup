﻿using System;
using System.Net.Http;

namespace BuildUp.Web
{
    public class GetBuildingsRequest : BaseWebRequest
    {
        public GetBuildingsRequest()
        {
            IsAuthorized = false;
            URL = "building";
            Method = HttpMethod.Get;
        }
    }
}
