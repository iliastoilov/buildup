﻿using System;
using System.Net.Http;

namespace BuildUp.Web
{
    public class GetMaterialsRequest : BaseWebRequest
    {
        public GetMaterialsRequest()
        {
            IsAuthorized = false;
            URL = "material";
            Method = HttpMethod.Get;
        }
    }
}
